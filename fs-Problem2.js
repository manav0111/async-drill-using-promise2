/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/


const { reject } = require('async');
const fs=require('fs');
const path = require('path');




// 1. Read the given file lipsum.txt

const ReadFile=()=>{

    
    return new Promise((resolve,reject)=>{
        const filepath=path.join(
            __dirname,
            "./lipsum.txt"
        )

        console.log(filepath);
        fs.readFile(filepath,'utf8',(err,data)=>{
            if(err)
            {
                reject(err);
            }
            else
            {
             
                resolve(data);
        
            }
        })

    })


}

// ReadFile();

// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
 



const uppercase=(data)=>{
   
      console.log(data);

      let uppercaseData=data.toUpperCase();
      console.log(uppercaseData);

      return new Promise((resolve,reject)=>{
          fs.writeFile('newFile.txt',uppercaseData,(err)=>{
            if(err)
            {
                
                console.log(err)
                reject(err);
                return;
            }
            else
            {
                console.log("New File Created and uppercase Data is inseted");
                resolve("newFile.txt");
            }
    
    
          });
          
      })

      

        
    

}

// uppercase();

// Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

const lowerCase=(data)=>{
        
        console.log(data);

        let lowerCaseText=data.toLowerCase();

        const sentences=lowerCaseText.split(' ');
        console.log("lowecase",sentences);

        let index=0;
        for(index=0; index<sentences.length; index++)
        {
            filepath=path.join(
                __dirname,
                "test",
                "./newFile1.txt"
            )

          
            
            fs.appendFile(filepath,`,${sentences[index]}`,(err)=>{
                console.log("Data inserted In newFile1.txt ",);
            })
        }

        return new Promise((resolve,reject)=>{
            if(index==sentences.length)
            {
                resolve(",newFile1.txt");
            }
            else
            {
                reject("File Not Created for Lower Case");
            }
        })
    
}

// lowerCase();

// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

const SortData=()=>{

   let filepath=path.join(
        __dirname,
        "/test",
        "newFile.txt"
    )

    console.log(filepath);

    return new Promise((resolve,reject)=>{

    let sorteddata=new Promise((resolve,reject)=>{

        fs.readFile(filepath,'utf8',(err,data)=>{

            if(err)
            {
                reject(err);
            }
    
            const sentences=data.split(' ');
    
            console.log(sentences);
    
    
            const sorteddata=sentences.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
           
            if(sorteddata)
            {
                resolve(sorteddata);
            }
            console.log("sorted:",sorteddata);
        })
    })


    sorteddata.then((sorteddata)=>{
         let index=0;
            let str="";
            for( index=0; index<sorteddata.length; index++)
            {
                
                str+=sorteddata[index];
                str+=",";
                console.log(sorteddata[index]);
               
            }


            fs.appendFile('./newFile2.txt',str,(err)=>{
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    console.log("Data inserted In newFile2.txt:",str);

                }
            })

            if(index==sorteddata.length)
            {
                resolve("newFile2.txt");
            }
    })


    })
}

// SortData();

const DeleteAll=()=>{
    let filepath=path.join(
        __dirname,
        "test",
        "filesname"
       
    );

    console.log(filepath);

    return new Promise((resolve,reject)=>{

        fs.readFile(filepath,'utf8',(err,data)=>{
    
            console.log(data);
    
            const FilesNames=data.split(',');

            let index=0;
            for(index=0; index<FilesNames.length;index++)
            {
                fs.unlink(FilesNames[index],(err)=>{
                    if(err)
                    {
                        reject(err);
                    }
                    console.log(`File is Deleted`);
                })
            }

            if(index==FilesNames.length)
            {
                resolve("All files Are SuccesFully Deleted");
            }


    
        })  
    })


}

// DeleteAll();

const AppendFileName=(text)=>{

    return new Promise((resolve,reject)=>{
        fs.appendFile('filesname',`${text},`,(err)=>{
            if(err)
            {
                reject(err);
            }
            else
            {
                resolve("FileName is appended");

            }
          })
        
    })

}


const fsProblem2=()=>{
    ReadFile().then((data)=>{
     
        console.log(data);

        return data;
    })
    .then((data)=>{
        uppercase(data)
        .then((filename)=>{
            AppendFileName(filename);
        });

        return data;
    })
    .then((data)=>{
        lowerCase(data)
        .then((filename)=>{
            AppendFileName(filename);
        })
        return data;
    })
    .then((data)=>{
        SortData()
        .then((filename)=>{
            AppendFileName(filename);
        })
        .then(()=>{
            setTimeout(()=>{
                DeleteAll()
            .then((msg)=>{
                console.log(msg);
            })
            },5000)
            
        })
    })
    .catch((err)=>console.log(err));
    
}


module.exports=fsProblem2;





