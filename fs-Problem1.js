// Using callbacks and the fs module's asynchronous functions, do the following:
// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously

const { reject } = require("async");
const fs = require("fs");
const path = require("path");

const fsProblem1 = (dirName, nooffiles) => {
  const MakeDir = new Promise((resolve, reject) => {
    fs.mkdir(dirName, (err) => {
      if (err) {
        reject(err);
        return;
      } else {
        console.log("Directory Created");

        resolve(nooffiles);
      }
    });
  });

  MakeDir.then(() => {
    const MakeFiles = new Promise((resolve, reject) => {
      const fileNamesArray = [];
      let cnt = 0;
      for (let i = 1; i <= nooffiles; i++) {
        const fileName = `file_${i}.json`;
        const filePath = path.join(dirName, fileName);
        const Data = { key: Math.random() };
        fileNamesArray.push(filePath);

        let filescreatedPromise = new Promise((resolve, reject) => {
          fs.writeFile(filePath, JSON.stringify(Data), (err) => {
            if (err) {
              reject(err);
            } else {
              cnt++;
              console.log(`${filePath} Created`);
              resolve(`${filePath} Created`);
            }
          });
        });

        filescreatedPromise
          .then(() => {
            if (cnt == nooffiles) resolve(fileNamesArray);
          })
          .catch((err) => console.log(err));
      }
    });

    MakeFiles.then((fileNamesArray) => {
      console.log(fileNamesArray);
      console.log("snknKS");

      let Deletecnt = 0;

      for (let index = 0; index < fileNamesArray.length; index++) {
        console.log(fileNamesArray[index]);
        fs.unlink(`${fileNamesArray[index]}`, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("sbjfbajkscv");
            Deletecnt++;
            console.log(`${fileNamesArray[index]} is Deleted`);
          }
        });
      }
    }).catch((err) => console.log(err));
  });
};

module.exports = fsProblem1;
